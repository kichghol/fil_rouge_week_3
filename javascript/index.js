
  let selectedArticle
  let selectedArticleCart
  let panier = new Cart ()
  const inventaire = new Inventory ()
  const user = new User("john","jimmy",50)


  document.addEventListener('DOMContentLoaded', () =>{
    let myHeaders = new Headers()
    myHeaders.append("Content-Type", "application/json")
    myHeaders.append("Accept", "application/json")
    
    article_list_API = []
    fetch('http://localhost:3000/inventories/1' , { headers: myHeaders, mode: 'cors'}) // on récupère les données de l'inventaire 1
    .then ( (response)=> {
      return response.json()
    })
    .then ( (data)=> {

      for(let i = 0;i<data.articles.length;i++){ // a chaque tour de boucle on convertit le modele de l'api dans celui de l'article et on l'ajoute à une liste
        // console.log(tmpArticle)
        article_list_API.push({nom:data.articles[i].name, quantity: data.articles[i].quantity , prix:  data.articles[i].price})
      }
      inventaire.listeArticles = article_list_API

      console.log(inventaire.listeArticles)
      // console.log(article_list_API)
    })
    .then (() => {
      updateInventory()

    })

    addButton()
    updateCart()
    clickTheArticle ()
    removeButton ()
    payButton()
  })


    

async function updateInventory() {

      //  on recupere l'element html grace a son id
      const inventaireHtml = document.getElementById('inventaire')
      inventaireHtml.innerHTML=""

      // fetch('http://localhost:3000/users/' , { headers: myHeaders, mode: 'cors'})
      // .then ( (response)=> {
      //   return response.json()
      // })
      // .then ( (data)=> {
      //   console.log(data)
      // })
      
      for (article of inventaire.listeArticles){
        const newArticle = document.createElement('tr')
        newArticle.classList.add("trInventaire")
        for (element in article){
          const newElement = document.createElement('td')
          newElement.innerHTML = article[element]
          newArticle.append(newElement)
        }
        inventaireHtml.append(newArticle)
      }
    
      
    
      clickArticle()
      clickTheArticle ()
  }

  function clickArticle (){
    const articles = document.getElementsByClassName('trInventaire')
    // on met let pour que sa selectionne 
    for (let i = 0; i<articles.length; i++){
      articles[i].addEventListener('click', () =>{
        for (let article of articles) {
          article.classList.remove("bg-primary")
        }
        articles[i].classList.add("bg-primary")
        const ajouter = document.getElementById('ajouter')
        ajouter.disabled = false      
        selectedArticle = i 
      })
    }
  }

  function addButton (){
    const ajouter = document.getElementById('ajouter')
    const price = document.getElementById('price')
    ajouter.addEventListener('click', () =>{
      panier.addArticle(inventaire.listeArticles[selectedArticle])      
      inventaire.removeQuantity(selectedArticle)
      price.innerHTML=panier.total

      updateCart()
      updateInventory()
    })
  }

  
  function updateCart() {

    //  on recupere l'element html grace a son id
  
    const panierHtml = document.getElementById('panier')
    panierHtml.innerHTML=""


    for (article of panier.listArticles){
      const selectedOne = document.createElement('tr')
      selectedOne.classList.add("trPanier")
      for (element in article){
        if(element != 'quantity'){
          const newElement = document.createElement('td')
          newElement.innerHTML = article[element]
          selectedOne.append(newElement)
        }
      
      }
      panierHtml.append(selectedOne)
    }
    
    clickTheArticle()
}


function clickTheArticle (){
  const articles = document.getElementsByClassName('trPanier')
  // on met let pour que sa selectionne 
  for (let i = 0; i<articles.length; i++){
    articles[i].addEventListener('click', () =>{
      for (let article of articles) {
        article.classList.remove("bg-primary")
      }
      articles[i].classList.add("bg-primary")
      const supprimer = document.getElementById('supprimer')
      supprimer.disabled = false      
      selectedArticleCart = i 
    })
  }

}

function removeButton (){
  const supprimer = document.getElementById('supprimer')
  const price = document.getElementById('price')
  supprimer.addEventListener('click', () =>{
  inventaire.addArticle(panier.listArticles[selectedArticleCart])      
  panier.removeArticle(selectedArticleCart)
  price.innerHTML=panier.total    
    updateCart()
    updateInventory() 
    
    
  })
}


function payButton(){
  const price = document.getElementById('price')

  document.getElementById("pay").addEventListener("click", () => {
    if (panier.total === 0) {
      alert("Votre panier est vide ... Vous venez d'acheter du rien pour 0€")
    }else if (user.checkPay(panier)) {
      alert("Félicitations pour votre achat de " + panier.total + "€, il vous reste " + user.budget + "€")
      panier.total = 0
      panier.listArticles = []
      updateCart()
      price.innerHTML=panier.total

    }
    else alert("vous n'avez qu'un budget de " + user.budget + "€ vous ne pouvez pas payer " + panier.total +"€")
  });
}