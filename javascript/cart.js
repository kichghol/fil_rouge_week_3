class Cart {
  constructor (){
    this.total = 0
    this.listArticles = []
  }
  addArticle(article){
    this.listArticles.push(article)
    this.total += article.prix
  }
  removeArticle(index){
    this.total -= this.listArticles[index].prix
    this.listArticles.splice(index,1)
  }


}

// Une classe Cart​ ayant une ​liste
//  d’articles​ et ​un total​. Depuis cette classe il sera possible ​d’ajouter ou de retirer​ un article du panier.