// Une classe Inventory​ ayant une liste de ​5 articles à 
// son instanciation​, uneméthode pour ​ajouter un article​ à 
// l’inventaire, une méthode pour​ enlever un articleet deux méthodes pour ​ajouter et 
// supprimer​ les quantités dans l’inventaire.

class Inventory {
  constructor(){
    this.listeArticles = [{nom:"salade", quantity:1, prix: 3},{nom:"couscous",quantity:3,prix: 4},{nom:"brick", quantity:3, prix: 4},
    {nom:"dafina", quantity:3, prix: 4},{nom:"tadjin", quantity:3, prix: 4},{nom:"pavlova", quantity:3, prix: 5}]
  }
  addArticle(article){
    for (let i = 0; i<this.listeArticles.length; i++) {

        if(this.listeArticles[i].nom === article.nom){
          this.addQuantity(i)
          return
        }
    }
    article.quantity=1
    this.listeArticles.push(article)
  }
  removeArticle(index){
    this.listeArticles.splice(index,1)
    
  }
  
  addQuantity(index){
    this.listeArticles[index].quantity += 1
  }

  removeQuantity(index){
    this.listeArticles[index].quantity -= 1
    // console.log(this.listeArticles[index].quantity )
    if(this.listeArticles[index].quantity === 0){
      this.removeArticle(index)
    }
  }
}


